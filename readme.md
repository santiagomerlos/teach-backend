# Backend Buid step by step

## Prerequisites

*   [nodejs](https://nodejs.org/en/) with __npm__
*   [mysql](https://dev.mysql.com/downloads/) 

    _**note**_ Important to install the shell tools for some tasks

*   [sequelize-cli](https://www.npmjs.com/package/sequelize-cli)

    for command line utilities for sequelize

    ```sh
    $ npm install -g sequelize-cli
    ```

## Steps

### Create project Directory

We use _be_ as project name for the back end you may use whatever you want

```sh
$ mkdir be 
$ cd be
```

*   Create directory for _scripts_

    ```sh
    $ mkdir scripts
    ```

*   Create directory for _sources_

    ```sh
    $ mkdir src
    ```

*   Create directory for _api_

    ```sh
    $ mkdir api
    ```

The final structure will be

    ```
    /
    +-  scripts/
    +-  src/
    +-  api/
    ```

### Initialize node project

```sh
$ npm init -y
```

*   Install dependencies

    *   [Sequelize](http://docs.sequelizejs.com/)

        [ORM](https://es.wikipedia.org/wiki/Mapeo_objeto-relacional) for data access to any modern database server. For this document we use _mysql_

        ```sh
        $ npm i -S sequelize mysql2
        ```

    *   [Swagger-tools](https://www.npmjs.com/package/swagger-tools)

        [Swagger](https://swagger.io/) is a specification form [REST](https://es.wikipedia.org/wiki/Transferencia_de_Estado_Representacional) messaging to a server


        ```sh
        $ npm i -S swagger-tools
        ```

    *   [Connect](https://github.com/senchalabs/connect#readme)

        Base middleware library for http servers

        ```sh
        $ npm i -S connect
        ```

*   Create Database Utility Scripts
    
    *   Create _db-reset.sql_ file

        Create a file on _scripts_ dir called _db-reset.sql_ with this content

        ```sql
        DROP SCHEMA IF EXISTS be_db;
        CREATE SCHEMA be_db;
        GRANT ALL PRIVILEGES ON be_db.* TO 'be_db'@'localhost' IDENTIFIED BY 'be_db';
        ```

        This is a script for create schema called __be_db__ you may use any name that fits to you

    *   Modify _package.json_ to create the reset database script

        ```json
        {
            ...
            "scripts": {
                "db-reset": "mysql -u root -p <./scripts/db-reset.sql"
            }
            ...
        }
        ```

    At this point the _package.json_ must look like

    ```json
    {
        "name": "be",
        "version": "1.0.0",
        "description": "",
        "main": "index.js",
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1",
            "db-reset": "mysql -u root -p <./scripts/db-reset.sql"
        },
        "keywords": [],
        "author": "",
        "license": "ISC",
        "dependencies": {
            "connect": "^3.6.6",
            "mysql2": "^1.6.1",
            "sequelize": "^4.38.0",
            "swagger-tools": "^0.10.4"
        }
    }
    ```

### Initialize Sequelize

Prepare structure for sequelize

```sh
$ cd src
$ sequelize init
$ cd ..
```

This create the structure for the sequelize ORM

After the comand the file structure will be

```
/
...
+-  src/
    +-  config/
        +-  config.json
    +-  migrations/
    +-  models/
        +-  index.js
    +-  seeders/
...    
```

Now you may create the migrations
    
### Create Model and Migrations

*   User table

    Create the user model and migration 

    ```sh
    $ cd src
    $ sequelize model:generate --name user --attributes name:string,email:string,password:string
    $ cd ..
    ```

    Now you may check the _**src/models/user.js**_ and _**src/migrations/*-create-user.js**_ to 
    verify the created files.

    #### Tune up the user model

    For a real life model you must to do some fine tune to the migration and model

    * for the migration code (/src/migrations/yyyymmddhhmmss-create-user.js)

    ```javascript
    'use strict';
    module.exports = {
        up: (queryInterface, Sequelize) => {
            return queryInterface.createTable('users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING
            },
            email: {
                type: Sequelize.STRING,
                /* THIS IS TO MAKE EMAIL UNIQUE*/
                unique: true 
            },
            password: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
            });
        },
        down: (queryInterface, Sequelize) => {
            return queryInterface.dropTable('users');
        }
    };
    ```

    And for the model code (_/src/model/user.js_)

    ```javascript
    'use strict';
    module.exports = (sequelize, DataTypes) => {
    var user = sequelize.define('user', {
        name: DataTypes.STRING,

        /* For modify model */
        email: {
        type: DataTypes.STRING,
        unique: true
        },
        
        password: DataTypes.STRING
    }, {});
    user.associate = function(models) {
        /*
         associations can be defined here
         */
    };
    return user;
    };    
    ```

    __*Note*__ We run the _sequelize_ commands inside the _src_ directory for keep all the code 
    inside this folder. This aproach is a good practice.

*   Create migrate command for npm

    *   Modify sequelize config

        Modify the _/src/config/config.json_ file. For industrial code the config approach must be 
        modified but this will be done in other document.

        ```json
        {
            "development": {
                "username": "be_db",
                "password": "be_db",
                "database": "be_db",
                "host": "127.0.0.1",
                "dialect": "mysql"
            },
            "test": {
                "username": "be_db",
                "password": "be_db",
                "database": "be_db",
                "host": "127.0.0.1",
                "dialect": "mysql"
            },
            "production": {
                "username": "be_db",
                "password": "be_db",
                "database": "be_db",
                "host": "127.0.0.1",
                "dialect": "mysql"
            }
        }
        ```
    
    *   Modify _package.json_

        Add a script on _package.json_

        ```json
        {
            ...
            "scripts": {
                ...
                "db-migrate": "sequelize db:migrate --config ./src/config/config.json --migrations-path ./src/migrations"
            }
            ...
        }
        ```

        after the modification the file must look like

        ```json
        {
            "name": "be",
            "version": "1.0.0",
            "description": "",
            "main": "index.js",
            "scripts": {
                "test": "echo \"Error: no test specified\" && exit 1",
                "db-reset": "mysql -u root -p <./scripts/db-reset.sql",
                "db-migrate": "sequelize db:migrate --config ./src/config/config.json --migrations-path ./src/migrations"
            },
            "keywords": [],
            "author": "",
            "license": "ISC",
            "dependencies": {
                "connect": "^3.6.6",    
                "mysql2": "^1.6.1",
                "sequelize": "^4.38.0",
                "swagger-tools": "^0.10.4"
            }
        }
        ```

    *   Run the migration

        ```sh
        npm run db-migrate
        ```

        This commands runs the migrations

        **_Note_** All the code related actions will be done throwght **_npm scripts_**

### Swagger API file

We now create a basic swagger file to create an user. In this case we use a _JSON_

create a file called _api.json_ on _/api_ folder

```json
{
    "swagger": "2.0",
    "info": {
        "title": "Demo",
        "version": "1.0.0"
    },
    "produces": [
        "application/json"
    ],
    "basePath": "/api",
    "paths": {
        "/version": {
            "x-swagger-router-controller": "versionController",
            "get": {
                "operationId": "getVersion",
                "responses": {
                    "default": {
                        "description": "error messages",
                        "schema": {
                            "$ref": "#/definitions/error"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "error": {
            "properties": {
                "message": {
                    "type": "string",
                    "default": "Invalid Request"
                }
            }
        },
        "login": {
            "properties": {
                "email": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        }
    }
}
```

This is a minimal learning structure to prepare the backend. 

This only will respond to a address

```
GET: /api/version
```

### Create the Back End Server

*   Create a file _index.js_ on the root of the _/src_ folder as the next one

    ```javascript
    'use strict';

    var connect = require('connect');
    var http = require('http');
    var swaggerTools = require('swagger-tools');
    var path = require('path');

    var app = connect();
    var serverPort = 3000;

    var options = {
    controllers: path.resolve(__dirname, './controllers'),
    useStubs: process.env.NODE_ENV === 'development' ? true : false
    };

    var swaggerDoc = require('../api/api.json');

    swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
        /*
        Interpret Swagger resources and attach metadata to request 
        - must be first in swagger-tools middleware chain
        */
        app.use(middleware.swaggerMetadata());

        /*
        Validate Swagger requests
        */
        app.use(middleware.swaggerValidator());
        
        /*
        Route validated requests to appropriate controller
        */
        app.use(middleware.swaggerRouter(options));
        
        /*
        Serve the Swagger documents and Swagger UI
        */
        app.use(middleware.swaggerUi());
        
        /*
        Start the server
        */
        http.createServer(app).listen(serverPort, function () {
            console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
        });
    });
    ```

*   Create controller folder

    ```sh
    $ cd src
    $ mkdir controllers
    $ cd ..
    ```

*   Create Version Controller
    
    A controller is a module that handles the calls that comes from a router for the server

    create a file in __/src/controllers__ named __versionController.js__

    ```javascript

    'use strict';

    module.exports.getVersion = function getVersion(req, res){
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify({
            version: '1.0.0'
        }, null, 2));
    }

    ```

*   Add script to start the server

    Modify __package.json__ as follows

    ```json
    ...
    "scripts": {
        ...
        "start": "node ./src/index.js"
        ...
    }
    ...
    ```

    To start the server do

    ```sh
    $ npm start
    ```

    After start you may access the version in a browser type the url

    ```
    http://localhost:3000/api/version
    ```

### Create user actions

Just for learn porpouses we only implements the create user

*   Add route for create user

    modify __api.json__ for add the action

    ```json
    {
        "swagger": "2.0",
        "info": {
            "title": "Demo",
            "version": "1.0.0"
        },
        "produces": [
            "application/json"
        ],
        "consumes": [
            "application/json"
        ],
        "basePath": "/api",
        "paths": {
            "/version": {
                "x-swagger-router-controller": "versionController",
                "get": {
                    "operationId": "getVersion",
                    "responses": {
                        "default": {
                            "description": "error messages",
                            "schema": {
                                "$ref": "#/definitions/error"
                            }
                        }
                    }
                }
            },
            "/user": {
                "post": {
                    "x-swagger-router-controller": "userController",
                    "operationId": "postUser",
                    "parameters": [
                        {
                            "name": "data",
                            "in": "body",
                            "schema": {
                                "type": "object",
                                "required": [
                                    "name",
                                    "email",
                                    "password"
                                ],
                                "properties": {
                                    "name": {
                                        "type": "string"
                                    },
                                    "email": {
                                        "type": "string"
                                    },
                                    "password": {
                                        "type": "string"
                                    }
                                }
                            }
                        }
                    ],
                    "responses": {
                        "201": {
                            "description": "User created"
                        }
                    }
                }
            }
        },
        "definitions": {
            "error": {
                "properties": {
                    "message": {
                        "type": "string",
                        "default": "Invalid Request"
                    }
                }
            },
            "login": {
                "properties": {
                    "email": {
                        "type": "string"
                    },
                    "password": {
                        "type": "string"
                    }
                }
            }
        }
    }
    ```

    Take a look of [this](https://swagger.io/docs/specification/2-0/describing-request-body/)

*   Add controller for create user

    add file _userController.js_ on the folder _/src/controllers/

    ```javascript
    'use strict';

    var models = require('../models');

    module.exports.postUser = function postUser(req, res) {
        res.setHeader('Content-Type', 'application/json');

        models.user.create(req.swagger.params.data.value)
            .then(function(user){
                res.end(JSON.stringify(user, null, 2));
            })
            .catch(function(err) {
                res.statusCode = 400;
                res.end(JSON.stringify(err, null, 2));
            });
    };
    ```

*   To test this just post a json to the address

    ```
    POST: api/users
    ```

    with data 

    ```json
    {
	    "name":"Juan Riera",
	    "email": "jujorie@hotmail.com",
	    "password": "123456"
    }
    ```

## Things To Do

*   There is no linters on the code
*   There is no a good error trap for not defined verbs
*   There is no good documentations on the swagger file
*   There is no good logger, maybe use morgan