DROP SCHEMA IF EXISTS be_db;
CREATE SCHEMA be_db;
GRANT ALL PRIVILEGES ON be_db.* TO 'be_db'@'localhost' IDENTIFIED BY 'be_db';
