'use strict';

var connect = require('connect');
var http = require('http');
var swaggerTools = require('swagger-tools');
var path = require('path');

var app = connect();
var serverPort = 3000;

var options = {
  controllers: path.resolve(__dirname, './controllers'),
  useStubs: process.env.NODE_ENV === 'development' ? true : false
};

var swaggerDoc = require('../api/api.json');

swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {
  /*
  Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  */
  app.use(middleware.swaggerMetadata());

  /*
  Validate Swagger requests
  */
  app.use(middleware.swaggerValidator());
  
  /*
  Route validated requests to appropriate controller
  */
  app.use(middleware.swaggerRouter(options));
  
  /*
  Serve the Swagger documents and Swagger UI
  */
  app.use(middleware.swaggerUi());
  
  /*
  Start the server
  */
  http.createServer(app).listen(serverPort, function () {
    console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
  });
});