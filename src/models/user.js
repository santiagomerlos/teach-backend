'use strict';
module.exports = (sequelize, DataTypes) => {
  var bcrypt = require('bcrypt');

  function cryptPassword(user) {
    return new Promise((resolve, reject) => {
      if (user.changed('password')) {
        bcrypt.hash(user.password, 10, (err, data) => {
          if (err) {
            reject(err);
          } else {
            user.password = data;
            resolve();
          }
        })
      } else {
        resolve();
      }
    })
  }

  var user = sequelize.define(
    'user',
    {
      name: DataTypes.STRING,

      /* For modify model */
      email: {
        type: DataTypes.STRING,
        unique: true
      },

      password: DataTypes.STRING
    },
    {
      hooks: {
        beforeCreate: (user, options) => {
          return cryptPassword(user);
        },
        beforeUpdate: (user, options) => {
          return cryptPassword(user);
        }
      }
    }
  );
  user.associate = function associate(models) {
    // associations can be defined here
  };

  user.checkCredentials = function checkCredentials({email, password}) {
    return user
      .findOne({
        where: {
          email
        }
      })
      .then((foundUser)=>{
        if(!foundUser){
          return Promise.reject(new Error('User Not found'));
        } else {
          return foundUser
            .isMatch(password)
            .then((success)=>{
              if(success) {
                return foundUser;
              }
              return null;
            })
        }
      })
  }

  user.prototype.isMatch = function isMatch(password) {
    var userPassword = this.password;
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, userPassword, (err, same) => {
        if (err) {
          reject(err)
        } else {
          resolve(same)
        }
      })
    })
  }

  return user;
};