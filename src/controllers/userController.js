'use strict';

var models = require('../models');

module.exports.postUser = function postUser(req, res) {
    res.setHeader('Content-Type', 'application/json');

    models.user.create(req.swagger.params.data.value)
        .then(function (user) {
            res.end(JSON.stringify(user, null, 2));
        })
        .catch(function (err) {
            res.statusCode = 400;
            res.end(err.toString());
        });
};

module.exports.login = function login(req, res) {

    models.user
        .checkCredentials(req.swagger.params.data.value)
        .then((user) => {
            if (user) {
                res.end(JSON.stringify(user, null, 2));
            } else {
                res.statusCode = 400;
                res.end(JSON.stringify({error: 'Invalid logon'}, null, 2));
            }
        })
        .catch((err) => {
            res.statusCode = 400;
            res.end(err.toString());
        })
}
